#-------------------------------------------------------------------#
# 	Pre-Requisit: Ensure required Data Created
#  	Product Area: Send Swag
# 	Story: Testing Automation
#------------------------------------------

##Including all Page Services
require './services/PageService/ContactService.rb'
require './services/PageService/SwagIqOrderService.rb'

describe "Send Swag - Verify Contact with blank phone number can't send the swag", :type => :request do

	contact_first_name = "TestUser"
	contact_last_name = "SwagIQ"
	contact_title = "AutomationTester"
	contact_address_mailing_street = "620 SW 5th Avenue Suite 40"
	contact_address_mailing_city = "NewYork"
	contact_address_mailing_state = "SH"
	contact_address_mailing_zip_code = "7718-07-01"
	contact_address_mailing_country = "USA"
	contact_phone_number = "(503) 421-7800"
	
	validation_found_msg = "Validation for blank phone number on send swag:"
	confirmation_msg_found = "Send swag confirmation message assertion:"
	gift_info_coffee_mug = "Coffee Mug"
	
	include_context "Login_Before_All"
	 
	before :all do
	   #Prerequisites action required
	   
	   # puts contact_creation
	   # SFL.open_all_tabs
	   # SFL.open_tab $con_tab_contacts
	   # puts "cicking on new button"
	   # SFL.click_button $sfl_new_button
	   
	   #Setting values for contact object on ui
	   # CON.set_first_name contact_first_name
	   # CON.set_last_name contact_last_name
	   # CON.set_title_value contact_title
	   # CON.set_address_mailing_street contact_address_mailing_street
	   # CON.set_address_mailing_city contact_address_mailing_city
	   # CON.set_address_mailing_state contact_address_mailing_state
	   # CON.set_address_mailing_zipcode contact_address_mailing_zip_code
	   # CON.set_address_mailing_country contact_address_mailing_country
	   # CON.set_phone contact_phone_number
	   # SFL.click_button $sfl_save_button
	   
	   contact_creation = "Contact con = new Contact(LastName='SwagIQ',FirstName = 'TestUser',
Title = 'AutomationTester',MailingStreet = '620 SW 5th Avenue Suite 40',
MailingCity = 'NewYork',MailingState = 'SH',MailingPostalCode = '7718-07-01',
MailingCountry = 'USA',Phone = '(503) 421-7800'); insert con;"

	   AL.execute_apex_script contact_creation
	end
	
		it "TestSetp: TS-01:  Validation message Should appear on send swag for contact with blank phone number." do
			SFL.open_all_tabs
			SFL.open_tab $con_tab_contacts
			SFL.click_button_go
			CON.open_contact contact_first_name, contact_last_name
			SFL.click_button $sfl_edit_button
			CON.set_phone ""
			SFL.click_button $sfl_save_button
			CON.click_on_button_send_swag
			CON.select_contact_Address
			CON.click_use_this_address_buttons
			
			#Asserting validation message 
			validation_assertion = UTIL.verify_text_on_page $con_send_swag_validation_msg_phone_is_required
			assert_values_and_report_result true , validation_assertion , "Send Swag" + validation_found_msg 
		end
		
		it "TestStep: TS-02: Check  contact after setting phone number value able to send swag.." do
			login_salesforce
			SFL.open_all_tabs
			SFL.open_tab $con_tab_contacts
			SFL.click_button_go
			CON.open_contact contact_first_name, contact_last_name
			SFL.click_button $sfl_edit_button
			CON.set_phone contact_phone_number
			SFL.click_button $sfl_save_button
			
			CON.click_on_button_send_swag
			CON.select_contact_Address
			CON.click_use_this_address_buttons 
			CON.select_coffee_mug
			CON.click_canvas_ui_send_swag_button

			assert_confirmation_msg = CON.verify_send_swag_confirmation_msg
			assert_values_and_report_result true , assert_confirmation_msg , "Send Swag" + confirmation_msg_found 
			CON.click_canvas_ok
	
			UTIL.page_refresh
	
			assert_value = CON.verify_swag_iq_order_status_under_related_list gift_info_coffee_mug, $swiqodr_status_order_confirmed
			assert_values_and_report_result true , assert_value , "Send Swag" + confirmation_msg_found 
		end
	after :all do
		##Destroying created data for testing.
		delete_contact = "delete [SELECT Id FROM Contact WHERE LastName = 'SwagIQ'];"
		login_salesforce
		AL.execute_apex_script delete_contact
	end
end