#-------------------------------------------------------------------#
# 	Pre-Requisit: Ensure base data is loaded
#  	Product Area: Send SwagIQ Order
# 	Story: Testing Automation
#------------------------------------------

##Including all Page Services
require './services/PageService/ContactService.rb'
require './services/PageService/ContactService.rb'

describe "SwagIqOrder - Verify SwagIqOrder generation on Contact Title Change using Auto Send Rule", :type => :request do

	contact_first_name = "TestUser"
	contact_last_name = "SwagIQ"
	contact_title = "AutomationTester"
	contact_address_mailing_street = "620 SW 5th Avenue Suite 40"
	contact_address_mailing_city = "NewYork"
	contact_address_mailing_state = "SH"
	contact_address_mailing_zip_code = "7718-07-01"
	contact_address_mailing_country = "USA"
	contact_phone_number = "(503) 421-7800"
	
	validation_found_msg = "Validation for blank phone number on send swag:"
	confirmation_msg_found = "Send swag order with in progress status created:"
	
	include_context "Login_Before_All"
	 
	before :all do
	   #Prerequisites action required
	   SFL.open_all_tabs
	   SFL.open_tab "Contacts"
	   puts "cicking on new button"
	   SFL.click_button $sfl_new_button
	   
	   #Setting values for contact object on ui
	   CON.set_first_name contact_first_name
	   CON.set_last_name contact_last_name
	   CON.set_title_value contact_title
	   CON.set_address_mailing_street contact_address_mailing_street
	   CON.set_address_mailing_city contact_address_mailing_city
	   CON.set_address_mailing_state contact_address_mailing_state
	   CON.set_address_mailing_zipcode contact_address_mailing_zip_code
	   CON.set_address_mailing_country contact_address_mailing_country
	   CON.set_phone contact_phone_number
	   SFL.click_button $sfl_save_button
	end
	
	it "TestSetp: TS-01:  Validation message Should appear on send swag for contact with blank phone number." do
		SFL.open_all_tabs
		SFL.open_tab "SwagIQ Tools"
		SWQR.edit_swag_iq_rule "Contact Title Change"
		SWQR.set_rule_active
		SFL.click_button $sfl_save_button
		
		SFL.open_all_tabs
		SFL.open_tab "Contacts"
		CON.open_contact contact_first_name, contact_last_name
		SFL.click_button $sfl_edit_button
		CON.set_title_value "MannualTester"
		SFL.click_button $sfl_save_button
		UTIL.page_refresh
		
		assert_order_status = CON.verify_swag_iq_order_status_under_related_list "Coffee Mug", "In Progress"
		assert_values_and_report_result true , assert_order_status , "Send Swag" + confirmation_msg_found 
		
		CON.open_swag_iq_order_in_related_list "Coffee Mug", "In Progress"
		
		send_mechanism_value = SWIQODR.get_field_value_for_label $swiqodr_label_send_mechanism
		assert_values_and_report_result "Auto Send" , send_mechanism_value , "Send Swag" + validation_found_msg
		
		reason_value = SWIQODR.get_field_value_for_label $swiqodr_label_send_mechanism
		assert_values_and_report_result "Auto Send" , reason_value , "Send Swag" + validation_found_msg
	end
end