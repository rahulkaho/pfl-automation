#-------------------------------------------------------------------#
# 	Pre-Requisit: Ensure base data is loaded
#  	Product Area: Send SwagIQ Order
# 	Story: Testing Automation
#------------------------------------------

##Including all Page Services
require './services/PageService/ContactService.rb'
require './services/PageService/SwagIqOrderService.rb'
require './services/PageService/SwagIqToolsService.rb'

describe "SwagIqOrder - Verify SwagIqOrder generation on Contact Title Change using Auto Send Rule", :type => :request do

	contact_first_name = "TestUser"
	contact_last_name = "SwagIQ"
	contact_title = "AutomationTester"
	contact_address_mailing_street = "620 SW 5th Avenue Suite 40"
	contact_address_mailing_city = "NewYork"
	contact_address_mailing_state = "SH"
	contact_address_mailing_zip_code = "7718-07-01"
	contact_address_mailing_country = "USA"
	contact_phone_number = "(503) 421-7800"
	
	validation_found_msg = "Validation for blank phone number on send swag:"
	confirmation_msg_found = "Send swag order with in progress status created:"
	auto_send_found_msg = "Send swag order send mechanism auto send:"
	reason_found_msg = "Send swag order reason:"
	gift_info_coffee_mug = "Coffee Mug"
	delete_contact = "delete [SELECT Id FROM Contact WHERE LastName = 'SwagIQ'];"
	
	contact_creation = "Contact con = new Contact(LastName='SwagIQ',FirstName = 'TestUser',
MailingStreet = '620 SW 5th Avenue Suite 40',
Title = 'AutomationTester',MailingStreet = '620 SW 5th Avenue Suite 40',
MailingCity = 'NewYork',MailingState = 'SH',MailingPostalCode = '7718-07-01',
MailingCountry = 'USA',Phone = '(503) 421-7800'); insert con;"
	
	include_context "Login_Before_All"
	 
	before :all do
	   #Prerequisites action required
	   # SFL.open_all_tabs
	   # SFL.open_tab $con_tab_contacts
	   # puts "cicking on new button"
	   # SFL.click_button $sfl_new_button
	   
	   #Setting values for contact object on ui
	   # CON.set_first_name contact_first_name
	   # CON.set_last_name contact_last_name
	   # CON.set_title_value contact_title
	   # CON.set_address_mailing_street contact_address_mailing_street
	   # CON.set_address_mailing_city contact_address_mailing_city
	   # CON.set_address_mailing_state contact_address_mailing_state
	   # CON.set_address_mailing_zipcode contact_address_mailing_zip_code
	   # CON.set_address_mailing_country contact_address_mailing_country
	   # CON.set_phone contact_phone_number
	   # SFL.click_button $sfl_save_button
	   
	    AL.execute_apex_script contact_creation
	end
	
	it "TestSetp: TS-01:  Validation message Should appear on send swag for contact with blank phone number." do
		SFL.open_all_tabs
		SFL.open_tab $con_tab_contacts
		SFL.click_button_go
		CON.open_contact contact_first_name, contact_last_name
		SFL.click_button $sfl_edit_button
		CON.set_title_value "MannualTester"
		SFL.click_button $sfl_save_button

		UTIL.page_refresh

		assert_order_status = CON.verify_swag_iq_order_status_under_related_list "Coffee Mug", "Order Confirmed"
		assert_values_and_report_result true , assert_order_status , "Send Swag" + confirmation_msg_found 

		CON.open_swag_iq_order_in_related_list gift_info_coffee_mug, $swiqodr_status_in_progress
		
		send_mechanism_value = SWIQODR.get_field_value_for_label $swiqodr_label_send_mechanism
		assert_values_and_report_result $swqr_select_value_auto_send , send_mechanism_value , "Send Swag" + auto_send_found_msg
	end
	after :all do
		##Destroying created data for testing.
		login_salesforce
		AL.execute_apex_script delete_contact
	end
end