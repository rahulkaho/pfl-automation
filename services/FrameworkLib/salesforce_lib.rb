#Copyright PFL, All rights reserved.
#Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
#result in criminal or other legal proceedings.

module SFL
extend RSpec::Matchers
extend Capybara::DSL

#Locators
$sfl_app_button = '#tsidButton'
$sfl_app_label = '#tsidLabel'
$sfl_app_menu = '#tsidMenu'
$sfl_app_user_navigation_button = "//div[contains(@id, 'userNav-arrow')]"
$sfl_app_user_navigation_menu_items = '#userNav-menuItems'
$sfl_close_lighting_option = "//a[contains(@id, 'tryLexDialogX')]";
$sfl_all_tabs_img = "//img[contains(@title, 'All Tabs')]";
$sfl_all_tabs_heading ="//h1[contains(text(), 'All Tabs')]"
$sfl_logout = "Logout"
$sfl_setup = 'Setup'
$sfl_get_started =  "//input[contains(@title, 'Get Started')]"
$sfl_login = "Login"
$sfl_create = "Create"
$sfl_objects = "Objects"
$sfl_save_button = "Save"
$sfl_edit_button = "Edit"
$sfl_new_button = "New"
$sfl_go = "Go!"
$sfl_manage_user = "Manage Users"
$sfl_profiles = "Profiles"
$sfl_apex_job = "Apex Jobs"
$sfl_new_custom_obj = "//input[contains(@value, 'New Custom Object')]"
$sfl_page_layout_button_target_location = "//legend[contains(text(), 'Custom Buttons')]/..//div"
$sfl_swag_iq_send_batch_job_status = "//table[@class='list']//tr//td//a[contains(text(), 'SwagIQSendBatch')]/ancestor::tr[1]//td[3]"
$sfl_body_cell = "//*[@id='bodyCell']"
$sfl_select_page_layout_button ="//*[@id='troughCategory__Button']" 
$sfl_select_page_layout_field_section ="//*[@id='troughCategory__Field']" 
$sfl_page_layout_quick_search_field = "//input[contains(@id, 'quickfind')]"
$sfl_button_scroll_section = "//*[contains(@id, 'PLATFORM_ACTION')]"
$sfl_field_scroll_section = "//div[contains(@class, 'canvasDrop')]"
$sfl_field_drop_section = "//div[contains(@class, 'section fieldDrop')][2]"
$sfl_quick_global_search = "//input[contains(@class, 'quickFindInput')]"
$sfl_batch_job_list = "//table[@class='list']//tr"
$sfl_inside_body = "//*[@id='bodyCell']"
$sfl_batch_job_status_in_progress = "Processing"
$sfl_batch_job_status_preparing = "Preparing"
$sfl_batch_job_status_holding = "Holding"
$sfl_batch_job_status_queued = "Queued"
#salesforce profile

############################################
#  general methods
############################################
	 
	 #(select_app,  Method will select application from salesforce UI)
	 # @param appname, the name of application want to select would pass an parameter.
	 # @return null
	 ##
	def SFL.select_app appname
		selectedApp = find($app_button).find($sfl_app_label).text
		if (appname !=selectedApp)
			find($app_button).click
			UTIL.min_wait
			find($sfl_app_menu).find("#tsid-menuItems").find(:link,"#{appname}").click
			UTIL.page_load_waiting
		end
	end
	
	#(open_all_tabs, will click on all tab image "+")
	# @return null
	def SFL.open_all_tabs
		UTIL.rexecute_script_block do
			find(:xpath, $sfl_all_tabs_img).click
		end
		UTIL.wait_object_appear $sfl_all_tabs_heading
	end
	
	#(open_tab, will open required tab from the list of all tab)
	#@param tabname
	# @return null
	def SFL.open_tab tabname
		UTIL.rexecute_script_block do
			SFL.open_all_tabs
			find(:xpath,"//table[@class='detailList tabs']//a[contains(text(),'"+tabname+"')]").click
			UTIL.min_wait
			
			if(page.has_xpath?($sfl_close_lighting_option))
				find(:xpath, $sfl_close_lighting_option).click
			end
			
			element_found = page.has_xpath?($sfl_all_tabs_heading)
			if (element_found ==true)
			   raise "Not able to found tab:" + tabname
			else
				break
			end
		end
		UTIL.page_load_waiting
	end
	
	#(click_setup, will click on setup link to get display with salesforce setting)
	# @return null
	def SFL.click_setup
		UTIL.rexecute_script_block do
			find(:xpath, $sfl_app_user_navigation_button).click
			UTIL.min_wait
			click_link $sfl_setup
		end
		UTIL.wait_object_appear $sfl_get_started	
	end
	
	#(open_object, open salesforce object)
	#@param : object_name
	# @return null
	def SFL.open_object object_name
		SFL.click_setup
		UTIL.rexecute_script_block do
			click_link $sfl_create
		end
		UTIL.min_wait
		UTIL.rexecute_script_block do
			click_link $sfl_objects
		end
		UTIL.wait_object_appear $sfl_new_custom_obj
		UTIL.rexecute_script_block do
			click_link object_name
		end
		UTIL.min_wait
	end
	
	def SFL.clone_exsisting_profile profilename, clone_name
			SFL.click_setup
			click_link $sfl_manage_user
			UTIL.page_load_waiting
			click_link $sfl_profiles
			UTIL.page_load_waiting
			click_link profilename[0]
			
			if(page.has_xpath?($sfl_close_lighting_option))
				find(:xpath, $sfl_close_lighting_option).click
			end
			sleep $sf_wait_less
			gen_scroll_to "//a/span[text()='#{permission_set_name}']"
			find(:xpath,"//a/span[text()='#{permission_set_name}']").click
			SFL.click_button buttonName
			UTIL.page_load_waiting
			
		end
	end
	
	#Salesforce login
	def SFL.login userName , userPassword
		visit $SALESFORCE_URL
		fill_in $SALESFORCE_USERNAME, :with => userName
		fill_in $SALESFORCE_PASSWORD, :with => userPassword
		click_button $sfl_login
		if(page.has_xpath?($sfl_close_lighting_option))
			find(:xpath, $sfl_close_lighting_option).click
		end
		UTIL.page_load_waiting
	end
	
	#logout from current user
	def SFL.logout
		find(:xpath, $sfl_app_user_navigation_button).click
		find($sfl_app_user_navigation_menu_items).find(:link,$sfl_logout).click
		UTIL.page_load_waiting
	end
	
	##Salesforce global search
	def SFL.click_global_search_value searchValue
		SFL.click_setup
		UTIL.page_load_waiting
		find(:xpath, $sfl_quick_global_search).set searchValue
		click_link searchValue
		UTIL.min_wait
		UTIL.page_load_waiting
	end
	
	##Salesforce global search
	def SFL.wait_for_swag_iq_apex_job_to_finish
		job_completed = true
		while(job_completed) do
			SFL.click_global_search_value $sfl_apex_job
			UTIL.page_load_waiting
			Array rows_list = all(:xpath, $sfl_batch_job_list)
			job_completed = false
			status_value = find(:xpath, $sfl_swag_iq_send_batch_job_status).text
			if(status_value == $sfl_batch_job_status_in_progress || status_value == $sf_batch_process_status_preparing || status_value ==$sfl_batch_job_status_queued || status_value == $sfl_batch_job_status_holding)
				job_completed = true
			end
		end
	end

	def SFL.add_field_on_object_page_layout object_name, page_layout_name, field_name
		field_source_path = "//span[text()= '"+field_name+"']/.."
		page_layout_edit_path = "//*[text()= '"+page_layout_name+"']//../td[1]/a[1]"
		
		SFL.open_object object_name
	
		UTIL.rexecute_script_block do
			find(:xpath, page_layout_edit_path).click
		end
		within(:xpath, $sfl_body_cell) do
			find(:xpath, $sfl_select_page_layout_field_section).click
			UTIL.min_wait
			find(:xpath, $sfl_page_layout_quick_search_field).set(field_name)
			UTIL.min_wait
			UTIL.scroll_to $sfl_button_scroll_section
			if (field_name.size > 22)
				new_field_name = field_name[0..20]
				field_source_path = "//span[contains(text() ,'"+new_field_name+"')]/.."
			end
			UTIL.min_wait
			UTIL.scroll_to $sfl_button_scroll_section
			UTIL.rexecute_script_block do
				source = first(:xpath, field_source_path)
				target = find(:xpath, $sfl_field_drop_section)
				source.drag_to(target)
				puts "item droped"
				
				sleep 3
				# UTIL.min_wait
				
				if (page.has_xpath?("//div[contains(@class, 'canvasDrop')]//span[contains(text() , '"+field_name+"')]"))
					puts "Field :#{field_name} : added successfully on layout."
				else
					raise "Error: #{field_name} not added successfully on layout."
				end
			end
		end
		SFL.click_button $sfl_save_button
		UTIL.min_wait
	end

	def SFL.remove_field_object_page_layout object_name, page_layout_name, field_name
		page_layout_edit_path = "//*[text()= '"+page_layout_name+"']//../td[1]/a[1]"
		
		SFL.open_object object_name
	
		UTIL.rexecute_script_block do
			find(:xpath, page_layout_edit_path).click
		end
		within(:xpath, $sfl_body_cell) do
			UTIL.rexecute_script_block do
				source = find(:xpath, "//div[contains(@class, 'section fieldDrop')][2]//div[contains(@class, 'itemLabel')]//span[contains(text(), '"+field_name+"')]/..")
				target = find(:xpath, "//div[contains(@class, 'item used')]//span[contains(text(), '"+field_name+"')]")
				target.drag_to(target)
				puts "item droped"
				UTIL.min_wait
			end
			
			UTIL.rexecute_script_block do
				source = find(:xpath, "//div[contains(@class, 'section fieldDrop')][2]//div[contains(@class, 'itemLabel')]//span[contains(text(), '"+field_name+"')]/..")
				target = find(:xpath, "//div[contains(@class, 'item used')]//span[contains(text(), '"+field_name+"')]")
				source.drag_to(target)
				puts "item droped"
				UTIL.min_wait
			end
		end
		SFL.click_button $sfl_save_button
		UTIL.min_wait
	end

	def SFL.add_button_to_object_page_layout object_name, page_layout_name, button_name
		button_source_path = "//span[text()= '"+button_name+"']/.."
		page_layout_edit_path = "//*[text()= '"+page_layout_name+"']//../td[1]/a[1]"
		
		SFL.click_setup
		UTIL.rexecute_script_block do
			click_link $sfl_create
		end
		UTIL.min_wait
		UTIL.rexecute_script_block do
			click_link $sfl_objects
		end
		UTIL.wait_object_appear $sfl_new_custom_obj
		
		UTIL.rexecute_script_block do
			click_link object_name 
		end
		UTIL.min_wait
		
		UTIL.rexecute_script_block do
			find(:xpath, page_layout_edit_path).click
		end
		within(:xpath, $sfl_body_cell) do
			find(:xpath, $sfl_select_page_layout_button).click
			UTIL.min_wait
			find(:xpath, $sfl_page_layout_quick_search_field).set(button_name)
			UTIL.min_wait
			UTIL.scroll_to $sfl_button_scroll_section
			if (button_name.size > 22)
				new_button_name = button_name[0..20]
				button_source_path = "//span[contains(text() ,'"+new_button_name+"')]/.."
			end
			UTIL.min_wait
			
			UTIL.rexecute_script_block do
				source = first(:xpath, button_source_path)
				target = find(:xpath, $sfl_page_layout_button_target_location)
				source.drag_to(target)
				source.drag_to(target)
				UTIL.min_wait
				
				if (page.has_xpath?("//div[text() = '"+button_name+"']"))
					puts "Button :#{button_name} : added successfully on layout."
				else
					raise "Error: #{button_name} not added successfully on layout."
				end
			end
		end
		SFL.click_button $sfl_save_button
		UTIL.min_wait
	end
	
    # Method to click on salesforce general method
	def SFL.click_button buttonName
		UTIL.rexecute_script_block do
			first(:button, buttonName).click
		end
		UTIL.page_load_waiting
	end
############################################
# general browser related
############################################
	def SFL.alert_ok
		
	end

	def SFL.alert_cancel
		
	end
	
	def SFL.click_button_go
		UTIL.rexecute_script_block do
			click_button($sfl_go)
		end
		UTIL.page_load_waiting
	end
	
	def SFL.go_to_url url_suffix
		url_prefix = page.current_url.split('/')[0]
		url_host = page.current_url.split('/')[2]
		visit url_prefix + "//" + url_host + url_suffix
		SFL.wait_for_page_load
	end