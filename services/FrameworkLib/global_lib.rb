#Copyright PFL, All rights reserved.
#Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
#result in criminal or other legal proceedings.

require './services/FrameworkLib/utility_lib.rb'

#Connstant variables to be used 

#Set general variable values through enviormental variables or property file.
$SALESFORCE_URL = "https://login.salesforce.com"
$TEST_EXE_RESULT_FILE = "TestExecutionResult.txt"
$SCRIPT_REXECUTION_COUNT = 3
$GEN_MIN_WAIT = 5
$GEN_MAX_WAIT = 10
$GEN_MED_WAIT = 8
$SALESFORCE_USERNAME = "username"
$SALESFORCE_PASSWORD = "password"
$DEFAULT_PROPERTY_FILE_NAME ="uitest.run.properties"
$OBJECT_WAIT_LOOP_ITERATION = 15

SFUSER = ENV['SFUSER'] ? ENV['SFUSER'] : UTIL.get_property_value("sfuser")
SFPASS = ENV['SFPASS'] ? ENV['SFPASS'] : UTIL.get_property_value("sfpass")  
RESULT_FILE = ENV['RESULT_FILE'] ? ENV['RESULT_FILE'] : $TEST_EXE_RESULT_FILE
MAIL_RECIPIENT = ENV['MAIL_RECIPIENT'] ? ENV['MAIL_RECIPIENT'] : UTIL.get_property_value("mail.recipient")
USER_MAIL = ENV['NOTIFY_USER'] ? ENV['NOTIFY_USER'] : UTIL.get_property_value("mail.user_name") 
USER_PASSWORD = ENV['NOTIFY_PASSWORD'] ? ENV['NOTIFY_PASSWORD'] : UTIL.get_property_value("mail.user_password")


def login_salesforce
	visit $SALESFORCE_URL
	fill_in $SALESFORCE_USERNAME, with: SFUSER
	fill_in $SALESFORCE_PASSWORD, with: SFPASS
	click_button "Login"
	if(page.has_xpath?($sfl_close_lighting_option))
		find(:xpath, $sfl_close_lighting_option).click
	end
	UTIL.page_load_waiting
end

def assert_values_and_report_result expectedValue , actual_value , tstMsg
	if (expect(actual_value).to eql(expectedValue))
		puts "Comparison: #{tstMsg} : Passed."
	else
		raise "Comparison: #{tstMsg} : Failed."
	end 
end

shared_context "Login_Before_All" do
	before :all do
		login_salesforce
	end	
end

shared_context "Login_Before_Each" do
	before :each do
		login_salesforce
	end
end
