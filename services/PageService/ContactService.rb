#Copyright PFL, All rights reserved.
#Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
#result in criminal or other legal proceedings.

module CON
extend RSpec::Matchers
extend Capybara::DSL

###################################################
# CONtact object locators
###################################################
$con_first_name = "//label[contains(text(), 'First Name')]/../following-sibling::td[1]//input"
$con_last_name = "//label[contains(text(), 'Last Name')]/../following-sibling::td[1]//input"
$con_title = "//label[contains(text(), 'Title')]/../following-sibling::td[1]//input"
$con_email = "//label[contains(text(), 'Email')]/../following-sibling::td[1]//input"
$con_phone = "//label[contains(text(), 'Phone')]/../following-sibling::td[1]//input"
$con_address_mailing_city = "//label[contains(text(), 'Mailing City')]/../following-sibling::td[1]//input"
$con_address_mailing_country = "//label[contains(text(), 'Mailing Country')]/../following-sibling::td[1]//input"
$con_address_mailing_state = "//label[contains(text(), 'Mailing State/Province')]/../following-sibling::td[1]//input"
$con_address_mailing_street = "//label[contains(text(), 'Mailing Street')]/../following-sibling::td[1]//textarea"
$con_address_mailing_zipcode = "//label[contains(text(), 'Mailing Zip/Postal Code')]/../following-sibling::td[1]//input"
$con_send_swag_button = "//input[contains(@value, 'Send Swag')]"
$con_send_swag_select_address = "//span[contains(text(), 'Contact Address')]/ancestor::div[1]"
$con_send_swag_use_this_address_button = "//button[contains(text(), 'Other Address')]/..//button[1]"
$con_send_swag_coffee_mug_gift = "//div[contains(text(), 'Coffee Mug')]/../..//div[1]//img"
$con_on_canvas_ui_send_swag_button = "//button[contains(text(), 'Send Swag')]"
$con_set_product_filter = "//input[contains(@id, 'ProductFilter')]"
$con_canvas_outerframe = "//div[@id ='canvas']//iframe"
$con_canvas_innerframe = "//div[@id ='canvasapp']//iframe"
$con_canvas_ok = "//button[contains(text(), 'OK')]"
$con_within_body = "//*[@id='bodyCell']"
$con_swag_iq_order_related_list_section = "//h3[contains(text(), 'SwagIQ Order')]/ancestor::div[1]/../.."
##Messages and labels
$con_tab_contacts = "Contacts"
$con_send_swag_validation_msg_phone_is_required = "Phone is required."
$con_send_swag_confirmation_msg = "You will receive a follow-up task once it has been delivered."
#########################################
# Contact object setter and getter methods
##########################################
	def CON.set_first_name name_value
		UTIL.rexecute_script_block do
			find(:xpath, $con_first_name).set name_value
		end
		UTIL.min_wait
	end

	def CON.set_last_name name_value
		UTIL.rexecute_script_block do
			find(:xpath, $con_last_name).set name_value
		end
	end

	def CON.set_title_value title_value
		UTIL.rexecute_script_block do
			find(:xpath, $con_title).set title_value
		end
	end

	def CON.set_email_value mail
		UTIL.rexecute_script_block do
			find(:xpath, $con_email).set mail
		end
	end
	
	def CON.set_address_mailing_street street
		UTIL.rexecute_script_block do
			find(:xpath, $con_address_mailing_street).set street
		end
	end
	
	def CON.set_address_mailing_city city
		UTIL.rexecute_script_block do
			find(:xpath, $con_address_mailing_city).set city
		end
	end
	
	def CON.set_address_mailing_state state
		UTIL.rexecute_script_block do
			find(:xpath, $con_address_mailing_state).set state
		end
	end
	
	def CON.set_address_mailing_zipcode zipcode
		UTIL.rexecute_script_block do
			find(:xpath, $con_address_mailing_zipcode).set zipcode
		end
	end
	
	def CON.set_address_mailing_country country
		UTIL.rexecute_script_block do
			find(:xpath, $con_address_mailing_country).set country
		end
	end
	
	def CON.set_phone phone_value
		UTIL.rexecute_script_block do
			find(:xpath, $con_phone).set phone_value
		end
	end
	
	def CON.open_contact first_name, last_name
		UTIL.rexecute_script_block do
			click_link last_name[0]
			UTIL.min_wait
			within(find(:xpath, $con_within_body)) do
				find(:xpath, "//span[contains(text(), '"+first_name+"')]").click
			end
			UTIL.page_load_waiting
		end
	end
	
	def CON.click_on_button_send_swag
		UTIL.rexecute_script_block do
			find(:xpath, $con_send_swag_button).click
		end
		sleep 2
	end
	
	def CON.select_contact_Address
		UTIL.rexecute_script_block do
			find(:xpath, $con_send_swag_select_address).click
		end
		UTIL.min_wait
	end
	
	def CON.click_use_this_address_buttons
		UTIL.rexecute_script_block do
			find(:xpath, $con_send_swag_use_this_address_button).click
		end
		UTIL.page_load_waiting
		UTIL.med_wait # soon we will find out dynamic wait solution
	end
	
	def CON.select_coffee_mug
		UTIL.rexecute_script_block do
			within_frame(find(:xpath, $con_canvas_outerframe)) do
				within_frame(find(:xpath, $con_canvas_innerframe)) do
					find(:xpath, $con_send_swag_coffee_mug_gift).click
				end
			end
		end
		UTIL.page_load_waiting
		UTIL.med_wait # soon we will find out dynamic wait solution
	end
	
	def CON.click_canvas_ui_send_swag_button
		UTIL.rexecute_script_block do
			within_frame(find(:xpath, $con_canvas_outerframe)) do
				within_frame(find(:xpath, $con_canvas_innerframe)) do
					find(:xpath, $con_on_canvas_ui_send_swag_button).click
				end
			end
		end
		UTIL.page_load_waiting
	end
	
	def CON.set_product_filter product_value
		UTIL.rexecute_script_block do
			within_frame(find(:xpath, $con_canvas_outerframe)) do
				within_frame(find(:xpath, $con_canvas_innerframe)) do
					find(:xpath, $con_set_product_filter).set product_value
				end
			end
		end
		UTIL.page_load_waiting
		UTIL.max_wait # soon we will find out dynamic wait solution
	end
	
	def CON.click_canvas_ok
		UTIL.rexecute_script_block do
			within_frame(find(:xpath, $con_canvas_outerframe)) do
				within_frame(find(:xpath, $con_canvas_innerframe)) do
					find(:xpath, $con_canvas_ok).click
				end
			end
		end
		UTIL.med_wait
	end
	
	def CON.verify_send_swag_confirmation_msg
		UTIL.rexecute_script_block do
			within_frame(find(:xpath, $con_canvas_outerframe)) do
				within_frame(find(:xpath, $con_canvas_innerframe)) do
					return page.has_text? $con_send_swag_confirmation_msg
				end
			end
		end
	end
	
	def CON.verify_swag_iq_order_status_under_related_list gift_info , order_status
		UTIL.rexecute_script_block do
			within(find(:xpath, $con_swag_iq_order_related_list_section)) do
				return page.has_xpath? "//td[contains(text(), '"+order_status+"')]/ancestor::tr[1]//a[contains(text(), '"+gift_info+"')]/ancestor::tr[1]"
			end
		end
	end
	
	def CON.open_swag_iq_order_in_related_list gift_info , order_status
		UTIL.rexecute_script_block do
			within(find(:xpath, $con_swag_iq_order_related_list_section)) do
				find(:xpath, "//td[contains(text(), '"+order_status+"')]/ancestor::tr[1]//a[contains(text(), '"+gift_info+"')]/ancestor::tr[1]//th[1]//a").click
			end
		end
		UTIL.page_load_waiting
		UTIL.wait_object_appear $swiqodr_page_title
	end
end