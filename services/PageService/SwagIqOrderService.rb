#Copyright PFL, All rights reserved.
#Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
#result in criminal or other legal proceedings.

module SWIQODR
extend RSpec::Matchers
extend Capybara::DSL

###################################################
# Swag IQ Order object locators
###################################################


###################################################
# Swag IQ Order Messages and Labels
###################################################
$swiqodr_label_send_mechanism = "Send Mechanism"
$swiqodr_label_reason = "Reason"
$swiqodr_page_title = "//h2[contains(text(), ' Swag Order - SwagIQ')]"
$swiqodr_reason_contact_title_change = "Contact Title Change"

###################################################
# Swag IQ Order Status and labels
###################################################
$swiqodr_status_in_progress = "In Progress"
$swiqodr_status_order_confirmed = "Order Confirmed"
#########################################
# Swag IQ Order object setter and getter methods
##########################################
	def SWIQODR.get_field_value_for_label label_value 
		UTIL.rexecute_script_block do
			return find(:xpath, "//td[contains(text(), '"+label_value+"')]/..//div").text
		end
	end
end